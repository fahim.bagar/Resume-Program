# Resume Process

## Description
For everyone that has the same problems with me. Windows OS suddenly suspended svchost.exe, though Windows OS is not freezed, but cannot open or run any application because Windows suspends all new run application. This application list all suspended processes and resume it.

## Symptoms
```
* Windows is not hanged
* Cannot run any new applications (even with Administrator privilege)
* Task manager can be called but graph is stopped and application icons are missing (just white background)
* When task manager is closed, it doesn't dissapear but hanged with white background
```

## Specification
In my laptop, I use:
* Windows 8.1 (64 bit)
* Target Framework: Net Framework 4.5.2

## How To Run
To achieve optimum result, please run it with Administrator privilege. To run it you can called through
```
cmd.exe \K start <folder_location>\ResumeProgam.exe
```
or just clicked generated ResumeProgam.exe

## Acknowledgement
Everyone at [www.stackoverflow.com] (http://www.stackoverflow.com).